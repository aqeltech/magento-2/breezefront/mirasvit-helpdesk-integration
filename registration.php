<?php

declare(strict_types=1);

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'AQELTech_BreezeMirasvitHelpdesk',
    __DIR__
);

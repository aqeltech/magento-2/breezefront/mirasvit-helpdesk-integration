# Mirasvit Helpdesk Integration

## Description

This extension does its best to integrate all storefront features of Helpdesk extension form Mirasvit vendor.

## Installation

```bash
composer require aqeltech/module-breeze-mirasvit-helpdesk
bin/magento module:enable AQELTech_BreezeMirasvitHelpdesk
```
